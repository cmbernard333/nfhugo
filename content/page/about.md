---
title: Swords and Swagger
subtitle:
comments: false
---

Swords and Swagger has been host to many a character on the journey of the heroes. 

# Main Cast

## Hushed Fang
Meow!

## Svarri Renl&#246;d
Big, bearded, and boisterous. The Northman Svarri Renlöd, who hails from the Island of Ruathym, has yet to find his true life’s calling. Abandoned by a mother who set off adventure never to return, Svarri struggled to make a name for himself in Ruathym. Competitive, aggressive, and often drunk, Svarri stumbled from job to job hoping to find his true calling. Prone to bouts of uncontrollable rage, Svarri found himself exiled from his people after setting fire to his village. Eager to prove his worth to the world, Svarri took to the seas of Faerun to find fortune, adventure, and enough glory to return back to his homeland. The sea calls, and warrior’s rest awaits!

## Toddiken Mosley
What ho! Allow me to introduce myself: Toddiken Ashley Mosley, Half-Elf (technically 15/32 Elf) amateur cartographer, member of the Raven's Club, bon vivant, and Destined Hero...of ...Destiny! Yes. Hmm. I shall have to workshop that title. My humble beginnings.. uh, began in the Mayor's Mansion in the town of (I can't remember) on the Moonshae Isles. There at the prime, heroic age of 25, I received a vision of a Celestial being who bestowed upon me fantastical gifts! Well, _a_ gift. I can speak Celestial, but I expect the supernatural strength and neigh invulnerability to show up any day now. In the meantime I've set out to be the a world renowned hero. Because that's what ~~my father would want̶~~ I'm destined for!

# Credits

Main Theme, Extended Theme, and Transition Theme by Austin Bridge (abridge)

# Homebrew Stuff

## Challenge Initiative: A Round table variant

1. Everyone rolls (record initiative number). 
2. Highest goes first. 
3. Proceed left. 

If DM rolls highest, only one monster gets to act on their turn.
Since DM plays all monsters, on each player’s turn, the DM can choose to roll against the recorded initiative number for the player who is taking their turn with a monster that hasn’t acted (or has a turn to use). If a DM’s chosen creature’s initiative number meets or exceeds the targetted player's recorded initiative, the chosen creature acts immediately after the player who is currently taking a turn.
On the DM’s turn, one monster takes a turn. At the end of the round, any remaining monsters take their turns.

### A note about legendary actions/legendary reactions
Legendary actions/reactions can still work in this round table variant. They are intended, per the rules in the Dungeons and Dragons 5th edition Player's Handbook, to allow monsters to act after another creature's turn, so this would allow a monster with a legendary action to go without a challenge roll.

