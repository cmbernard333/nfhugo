---
title: Swords and Swagger Props
subtitle: As heard on the radio!
comments: false
---

# Episode 17 - A Whole Worg of Trouble
The Wanted Poster featuring Fang
<a href="https://imgur.com/zIAwP5w"><img src="https://i.imgur.com/zIAwP5w.png" title="source: imgur.com" /></a>

# Shadow of the Jolly Lord - The Holiday Special - Part 1
Character Sheets

- **Austin** ( [front](https://s.put.re/WLxm6VFn.jpeg), [back](https://s.put.re/a7Tedk9B.jpeg), [spells](https://s.put.re/QLfxKEND.jpeg) )
- **Christian** ( [front](https://s.put.re/U89Uw3PU.jpeg.jpeg), [back](https://s.put.re/fzhExDkT.jpeg.jpeg) )
- **Glenn** ( [front](https://s.put.re/ToX2x2U2.jpeg), [back](https://s.put.re/FjEcdvUJ.jpeg) ) 