---
title: Props page!
subtitle: As heard on the radio!
date: 2019-12-11
tags: [ "podcast", "page", "general" ]
comments: true
---

## Props page added!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Just added a props page featuring our first prop from Episode 17 - A Whole Worg of Trouble.

Check it out! [Props](/page/props/)
