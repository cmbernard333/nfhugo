---
title: Holiday Episode Released!
subtitle: Swords and Swagger - The Holiday Special Part 1
date: 2019-12-27
tags: [ "release", "episode", "podcast" ]
comments: true
---

## Shadow of the Jolly Lord - Part 1 Released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

This release is the first part of a three parter of our holiday special. I plan to release two more parts in the coming weeks.

We are playing Shadow of the Demon Lord with Aj as the game master.

We do not go into great detail about this rules of the game as that would take up your valuable listening time. If you are interested in exploring the rules of Shadow of the Demon Lord, you can find the basic rules here on [drivethrurpg](https://www.drivethrurpg.com/product/156576/Victims-of-the-Demon-Lord-Starter-Guide&affiliate_id=514824) published by Schwalb Entertainment (pay what you want).

You can check out our [Props](/page/props/) page for our character sheets.

Be sure to check out Swords and Swagger wherever you get your podcasts! And leave us a nice review!

Follow us [@SwaggerSwords](https://twitter.com/swaggerswords)

**Additional Notes**

- More EQ tweaking. I believe this one is better than last week's episode.
- The website URL mentioned in the intro is wrong. We know. You are here aren't you?

As always, here is a direct link from anchor for you to sample!

{{< anchorfm "Shadow-of-the-Jolly-Lord---The-Holiday-Special---Part-1-e9rnql">}}
