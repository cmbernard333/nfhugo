---
title: Episode 16 Released!
subtitle: Swagger Shoppe 1
date: 2019-11-10
tags: [ "release", "episode", "podcast" ]
comments: true
---

## Episode 16 - Swagger Shoppe 1: Supplies and Stallion - Released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Yes that's only one Stallion.

Our heroes get their first ever interlude to do some much needed shopping and prepare for their journey to Amn. This type of episode will occur in the future between certain arcs as to allow the players to revitalize their swagger through shopping.

Be sure to check it out wherever you get your podcasts! And leave us a nice review!

Follow us [@SwaggerSwords](https://twitter.com/swaggerswords)

**Additional Notes**
We are now mixing down to mono and I think I finally figured out the EQ issue. I am by no means an audio expert so if you have any feedback please email us at swordsnswagger@gmail.com!

Also, here is a direct link from anchor for you to sample!

{{< anchorfm "Swagger-Shoppe-1-Supplies-and-Stallion---Episode-16-e8u273">}}
