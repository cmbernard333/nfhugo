---
title: Episode 17 Released!
subtitle: Worried about worgs!
date: 2019-12-08
tags: [ "release", "episode", "podcast" ]
comments: true
---

## Episode 17 - A Whole Worg of Trouble - Released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Episode 17 is the first episode starting our heroe's journey to Amn. One ploy fails to launch and the heroes end up in a race to the finish!

**Series Note**
The next episode of Swords and Swagger will not be part of the regular series. We are planning to do a record a holiday episode on the 22nd and will be playing
Shadow of the Demon Lord. Aj will be running the game and we will be having a guest playing with us. I expect that episode to be in a few parts, so stay tuned.

Be sure to check it Swords and Swagger wherever you get your podcasts! And leave us a nice review!

Follow us [@SwaggerSwords](https://twitter.com/swaggerswords)

**Additional Notes**
- We realize now that Oddman was not involved at all. We will have an answer for that in the future.
- Still working on the EQ. I am by no means an audio expert so if you have any feedback please email us at swordsnswagger@gmail.com!

As always, here is a direct link from anchor for you to sample!

{{< anchorfm "A-Whole-Worg-of-Trouble---Episode-17-e9dmjh">}}
