---
title: Holiday Episode Part 2 Released!
subtitle: Swords and Swagger - The Holiday Special Part 2
date: 2020-01-19
tags: [ "release", "episode", "podcast" ]
comments: true
---

## Shadow of the Jolly Lord - Part 2 Released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

This release is the final part of our holiday special! Thank you for your patience.

Our journey beneath the Shadow of the Demon Lord, with Aj as the game master, is violent, explosive, and exciting.

As a reminder, you can check out our [Props](/page/props/) page for our character sheets.

Be sure to check out Swords and Swagger wherever you get your podcasts! And leave us a nice review! 

Follow us [@SwaggerSwords](https://twitter.com/swaggerswords)

Thanks to everyone for following us and listening to our episodes. There would be no podcast without our loyal audience!

**Additional Notes**

- More EQ tweaking. I believe this one is better than last week's episode.
- Previously in this episode? Maybe I'll bring those back.

As always, here is a direct link from anchor for you to sample!

{{< anchorfm "Shadow-of-the-Jolly-Lord---The-Holiday-Special---Part-2-ea9rv9">}}
