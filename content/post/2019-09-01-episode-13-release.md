---
title: Episode 13 Released!
subtitle: Catch a wave!
date: 2019-09-15
tags: [ "release", "episode", "podcast" ]
---

## Episode 13 released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Episode 13 is released!

Be sure to check it out wherever you get your podcasts! And leave a review!

Also, here is a direct link from anchor for you to sample!

{{< anchorfm "Heroes-at-Sea---Episode-13-e5d1on">}}