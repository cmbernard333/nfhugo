---
title: Episode 12 Released!
subtitle: AKA we're back!
date: 2019-09-01
tags: [ "release", "episode", "podcast" ]
---

## Episode 12 released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Episode 12 is released! 

Be sure to check it out wherever you get your podcasts!

Also, here is a direct link from anchor for you to sample!

{{< anchorfm "Weathering-the-Storm---Episode-12-e56gmv">}}