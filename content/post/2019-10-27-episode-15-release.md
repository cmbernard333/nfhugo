---
title: Episode 15 Released!
subtitle: Whale Tale
date: 2019-10-27
tags: [ "release", "episode", "podcast" ]
comments: true
---

## Episode 15 - A Tale of a Whale - Released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Just as our heroes finally come ashore, they are faced with a massive shadowy creature from their past. Do they have what it takes to face down this fearsome foe? 

Listen and find out!

We are using a new initiative system, inspired by Schwalb Entertainment's [Shadow of the Demon Lord](https://schwalbentertainment.com/shadow-of-the-demon-lord/) called **Challenge Initiative**. See the [About Page](/page/about/) for details.

Be sure to check it out wherever you get your podcasts! And leave us a nice review!

Follow us [@SwaggerSwords](https://twitter.com/swaggerswords)

**Additional Credits**
Pirate Crew by Ross Bugden provided from https://youtu.be/hVBgKCYrI-c. Also on twitter [@rossbugden](https://twitter.com/RossBugden?s=17)

Also, here is a direct link from anchor for you to sample!

{{< anchorfm "A-Tale-of-a-Whale---Episode-15-e892pk">}}

