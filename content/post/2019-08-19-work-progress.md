---
title: New Website!
subtitle: Better than the old website!
date: 2019-08-19
tags: ["website", "launch"]
---

## Welcome to the new website!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger. 

Things are going to be in flux for awhile while I work on the new website. Bear with us here.

(Also, new episode recording is August 23 2019, so stay tuned.)