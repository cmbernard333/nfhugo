---
title: Episode 14 Released!
subtitle: A Bark from the Depths
date: 2019-09-29
tags: [ "release", "episode", "podcast" ]
---

## Episode 14 - A Bark from the Depths - released!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Episode 14 - A Bark from the Depths - is released!

Our heroes take a dive into the seas and face some vicious underwater denizens. Can they blub their way through this one? Listen and find out!

Be sure to check it out wherever you get your podcasts! And leave a review!

Also, here is a direct link from anchor for you to sample!

{{< anchorfm "A-Bark-from-the-Depths---Episode-14-e5lobb">}}