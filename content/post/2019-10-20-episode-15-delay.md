---
title: Episode 15 Delayed!
subtitle: Oh no!
date: 2019-10-20
tags: [ "episode", "podcast" ]
---

## Episode 15 - delayed!

Hey everybody, it's Christian Bernard your dungeon master for Swords and Swagger.

Episode 15 has been delayed to 10-26-2019! Thanks for bearing with us!