## Hey Everybody

Three would be heroes set off in search of adventure, glory, and a little bit of shiny. All too late they realize their choices determine the fate of the land.

Swords and Swagger is a real play podcast starring AJ as Svarri Renlod, Bryce as Fang, and Glenn as Toddiken Mosley. 

It releases bi-monthly.

Follow us on Twitter [@SwaggerSwords](https://twitter.com/SwaggerSwords)
